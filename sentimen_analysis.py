import time
import matplotlib.pyplot as plot
import pandas as pd
import seaborn as seaborn
from sklearn import metrics
from sklearn.feature_extraction.text import TfidfVectorizer as tfidf
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
#read the dataset
clean_train_data = pd.read_csv('clean_data_balance.csv', sep=',')

#show the diagram of sentiment
figure = plot.figure(figsize=(7, 5))
seaborn.countplot(data=clean_train_data, x='sentiment')
plot.show()

#show wordcloud each class of sentiment
from wordcloud import WordCloud
from nltk.corpus import stopwords
sentiments = ["negative", "positive"]
cloud = WordCloud(background_color="white", max_words=20, stopwords=stopwords.words('indonesian'))
def draw_word_clouds(dataframe):
    for i in sentiments:
        category = cloud.generate(dataframe.loc[dataframe['sentiment'] == i, 'text'].str.cat(sep='\n\n'))
        plot.figure(figsize=(5, 2.5))
        plot.imshow(category)
        plot.axis("off")
        plot.title(i)
        plot.show()
draw_word_clouds(clean_train_data)

#Split data become 2 part, train and validation
target = clean_train_data.sentiment
train_X_, validation_X_, train_y, validation_y = train_test_split(clean_train_data['text'], target, test_size=0.25,
                                                                  random_state=21)
# Vectorize the words by using TF-IDF Vectorizer - This is done to find how important a word in document is in comaprison to the data train
tfidf_vec = tfidf(min_df=3, max_features=None,
                  ngram_range=(1, 2), use_idf=1)
start = time.time()
train_X = tfidf_vec.fit_transform(train_X_)
end = time.time()
print("TFIDF finished in: ", end - start)

# Now we can run different algorithms to classify out data check for accuracy
# Classifier - Algorithm - Naive Bayes
# fit the training dataset on the classifier

model = MultinomialNB()
model.fit(train_X, train_y)
validation_X = tfidf_vec.transform(validation_X_)

# predict the labels on validation dataset
predicted = model.predict(validation_X)
expected = validation_y

# Use accuracy_score function to get the accuracy
print(metrics.classification_report(expected, predicted))
print(metrics.confusion_matrix(expected,predicted))

#Load data testing
clean_data_test = pd.read_csv('clean_data_test.csv', sep=',')
# Vectorize the words by using TF-IDF Vectorizer
test_X = tfidf_vec.transform(clean_data_test['text'])
# Load model and make prediction
test_predictions = model.predict(test_X)
output = pd.DataFrame({
    'text': clean_data_test['clean'],
    'sentiment': clean_data_test['sentiment'],
    'prediction': test_predictions
})
#Save to csv file
output.to_csv('Result-baru2.csv', index=False)
