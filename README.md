This project is task assigntment for completing job recruitment for Prosa.ai

# Description Task
Make Sentiment analysis base on restaurant review dataset

<!-- blank line -->
----
<!-- blank line -->
# Step by Step
<!-- blank line -->
----
<!-- blank line -->
## Data Collecting
This step data will be collected for processing needed
<!-- blank line -->
----
<!-- blank line -->
## Pre-processing
This step used to adjust the format of acquired data into the desired formatting. Processes involved in pre-processing steps are:
<!-- blank line -->
----
<!-- blank line -->
### Case Folding
Case folding used to unify the text formatting. Because not all text formatting is consistent with capital letter usage. 
Therefore, case folding is needed to change whole text into the standard form or to uniform text into lowercase.
<!-- blank line -->
----
<!-- blank line -->
### Remove Punctuation
Data cleaning is needed to clean special character existence in data, this proses used to reduce data noise.
<!-- blank line -->
----
<!-- blank line -->
### Method
This step will be implemented algorithm which author choose to do analyze from the data and create model
<!-- blank line -->
----
<!-- blank line -->
### Evaluate
This step is use to test and knowing the accuracy of this method and author get **84% accuracy** which is good enaugh result