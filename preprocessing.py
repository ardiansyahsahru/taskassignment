import string

import pandas as pd
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from nltk.corpus import stopwords
#load stop word
factory = StemmerFactory()
stemmer = factory.create_stemmer()
data = pd.read_csv('test_data_restaurant.tsv', sep='\t', names=["text", "sentiment"])

def clean_text(text):
    # lower text
    text = text.lower()
    # tokenize text and remove puncutation
    text = [word.strip(string.punctuation) for word in text.split(" ")]
    # remove words that contain numbers
    text = [word for word in text if not any(c.isdigit() for c in word)]
    # remove stop words
    stop = stopwords.words('indonesian')
    text = [x for x in text if x not in stop]
    text = " ".join(text)
    return text
data["clean"] = data["text"].apply(lambda x: clean_text(x))
data.to_csv("clean_data_test.csv") #save to csv file
clean_data = pd.read_csv("clean_data_test.csv", sep=",", skipinitialspace=True)
#remove short word
clean_data["clean"]=clean_data['clean'].str.split().map(lambda sl: " ".join(s for s in sl if len(s) > 3))

